#!/bin/bash

ERROR="\e[1;35mERROR:\e[0m"		#错误高亮
BULE="\e[1;36m"					#蓝色高亮
WHITE="\e[1;37m"				#白色高亮
white="\e[0;37m"				#白色
RED="\e[1;35m"					#红色
YELLOW="\e[1;31m"				#黄色高亮
yellow="\e[0;31m"				#黄色
YELLOW2="\e[0;32m"				#黄色2
WARNING="\e[0;33m"				#警告
COLOR="\e[0;33m"				#颜色变量，这里以WARNING初始化
CC="\e[0m"						#颜色截止

# 目录名称定义
ROOT_DIR=$(pwd)

# 文件名称定义
PCIIDS_GUIDE="use_guide.txt"
BUILD_GUIDE="build_guide.txt"
PATCH_LOG="patch.log"
BUILD_LOG="build.log"
MODULES_INSTALL_LOG="modules_install.log"
KERNEL_INSTALL_LOG="install.log"
DMESG_LOG="dmesg.log"
PATCH_VERIFY_LOG="verify.cat"

# sudo密码
PASSWD="123"

# Tools类型
if [ $2 ];then
	TOOLS_TYPE=$2
else
	TOOLS_TYPE='no'
fi

# 包管理安装
if [ $(which yum) ];then
	INSTALL="yum install -y"
elif [ $(which apt) ];then
	INSTALL="apt install -y"
fi

# 内核编译工具
apt_tools="make gcc bison flex libncurses5-dev  libelf-dev openssl libssh-dev bc"
yum_tools="make gcc bison flex ncurses-devel openssl-devel elfutils-libelf-devel"

# 检查软件是否安装
check_tools() {
	if [[ $1 == dpkg-source ]];then
		echo $PASSWD | sudo -S $INSTALL dpkg-dev
	fi
	echo $PASSWD | sudo -S $INSTALL $1
}

# 检查原始文件
check_files() {
	for i in $(ls $1)
	do 
		if [[ $i =~ "zxpm.sh" ]];then
			SHELL_FILE=$i
		elif [[ $i =~ "zip" ]];then
			if [[ $i =~ "All_Zhaoxin_Patch_for" ]];then
				PATCH_PACKAGE_NAME=$i
				PATCH_NAME=${PATCH_PACKAGE_NAME%.zip}
				TMP=${PATCH_PACKAGE_NAME##*V}
				PATCH_VERSION=${TMP%.*}
			else
				KERNEL_PACKAGE_TYPE='zip'
				KERNEL_PACKAGE_NAME=$i
			fi
		elif [[ $i =~ ".dsc" ]];then
				KERNEL_PACKAGE_TYPE='dsc'
				TSC_KERNEL_PACKAGE_NAME=$i
		elif [[ $i =~ ".tar." ]];then
			if [[ $i =~ ".orig.tar." ]];then
				TSC_ORIG_PACKAGE_NAME=$i
				KERNEL_PACKAGE_NAME=$i
			elif [[ $i =~ ".debian.tar." ]];then
				TSC_DEBIAN_PACKAGE_NAEM=$i
			else
				KERNEL_PACKAGE_TYPE='tar'
				KERNEL_PACKAGE_NAME=$i
			fi
		elif [[ $i =~ ".deb" ]];then
				KERNEL_PACKAGE_TYPE='deb'
				KERNEL_PACKAGE_NAME=$i
		elif [[ $i =~ ".src.rpm" ]];then
				KERNEL_PACKAGE_TYPE='rpm'
				KERNEL_PACKAGE_NAME=$i
		fi
	done
}

# 打印初始化信息
print_init_files() {
	echo -e "$BULE内核类型：$CC$KERNEL_PACKAGE_TYPE"
	if [ $KERNEL_PACKAGE_TYPE == 'dsc' ];then
		echo -e "$BULE内核文件："
		echo -e "$BULE    dsc文件：$CC$TSC_KERNEL_PACKAGE_NAME"
		echo -e "$BULE    orig文件：$CC$TSC_ORIG_PACKAGE_NAME"
		echo -e "$BULE    debian文件：$CC$TSC_DEBIAN_PACKAGE_NAEM"
	else
		echo -e "$BULE内核文件：$CC$KERNEL_PACKAGE_NAME"
	fi
	echo -e "$BULE脚本文件：$CC$SHELL_FILE"
	echo -e "$BULE参考补丁：$CC$PATCH_PACKAGE_NAME"
	echo -e "$BULE补丁版本：$CC$PATCH_VERSION"
	echo "------------------------------------------------------------"
}

# 备份内核和参考patch
backup_packages() {
	#1. 新建备份目录
	mkdir package_backup

	#2. 备份内核和参考patch
	if [ $KERNEL_PACKAGE_TYPE == "dsc" ];then
		mv $TSC_KERNEL_PACKAGE_NAME package_backup
		mv $TSC_ORIG_PACKAGE_NAME package_backup
		mv $TSC_DEBIAN_PACKAGE_NAEM package_backup
	else
		mv $KERNEL_PACKAGE_NAME package_backup
	fi
	mv $PATCH_PACKAGE_NAME package_backup
}

# 解压内核包
decompress_kernel() {
	mkdir kernel_orig
	case $KERNEL_PACKAGE_TYPE in
		'tar' )
			tar -xf package_backup/$KERNEL_PACKAGE_NAME -C kernel_orig/
			;;
		'zip' )
			check_tools unzip
			unzip package_backup/$KERNEL_PACKAGE_NAME -d kernel_orig/ >> /dev/null
			;;
		'dsc' )
			cd kernel_orig
			check_tools dpkg-source
			dpkg-source -x ../package_backup/$TSC_KERNEL_PACKAGE_NAME >> /dev/null
			rm $TSC_ORIG_PACKAGE_NAME
			cd ..
			;;
		'deb' )
			dpkg -x package_backup/$KERNEL_PACKAGE_NAME tmp >> /dev/null
			tar -xf tmp/usr/src/linux-source*/linux-source*.tar.* -C kernel_orig
			rm tmp/ -rf
			;;
		'rpm' )
			mkdir tmp
			cp package_backup/$KERNEL_PACKAGE_NAME tmp/
			cd tmp
			check_tools rpm2cpio
			check_tools	cpio
			rpm2cpio $KERNEL_PACKAGE_NAME |cpio --quiet -di
			tar -xf linux*.tar.* -C ../kernel_orig
			cd ..
			rm tmp -rf
			;;
		'' )
			;;
	esac
	KERNEL_SOURCE_NAME=$(ls kernel_orig)
	cp kernel_orig/* . -rf >> /dev/null
}

# 给内核源码打入初始补丁
patch_kernel() {
	#1. 解压参考patch
	check_tools unzip
	unzip package_backup/$PATCH_PACKAGE_NAME -d patch >> /dev/null
	cp package_backup/$PATCH_PACKAGE_NAME patch
	#2. 打补丁
	mkdir log
	cp patch/All_*/All_*/kernel_patch/All_Zhaoxin_Patch_for* .
	cd $KERNEL_SOURCE_NAME
	check_tools patch
	patch -p1 < ../All_Zhaoxin_Patch_for* | tee ../log/patch.log
	cd ..
	rm All_Zhaoxin_Patch_for*
}

# 打印patch信息
print_patch_files() {
	echo -e "$BULE当前补丁：$CC$PATCH_PACKAGE_NAME"
	echo -e "$BULE补丁文件：$CC$PATCH_NAME.patch"
	echo -e "$BULE补丁版本：$CC$PATCH_VERSION"
	echo "------------------------------------------------------------"
}

# 生成新的release note
renew_release_note() {
	rm $NEW_PATCH_NAME/*.htm*
	git clone -b $KERNEL_PACKAGE_TYPE https://gitee.com/liuxinux/patch_note.git -q
	mv patch_note/*.html $NEW_PATCH_NAME/
	rm patch_note -rf
	cd $NEW_PATCH_NAME
	mv ./*External* ./$EX_NOTE > /dev/null
	mv ./*Internal* ./$IN_NOTE > /dev/null
	RELEASE_DATE=$(date +%Y-%m-%d)
	sed -i "s/#DATE#/${RELEASE_DATE}/g" $EX_NOTE $IN_NOTE
	sed -i "s/#OS#/${OS_VERSION}/g" $EX_NOTE $IN_NOTE
	sed -i "s/#KERNEL#/${KERNEL_VERSION}/g" $EX_NOTE $IN_NOTE
	sed -i "s/#VERSION#/${PATCH_VERSION}/g" $EX_NOTE $IN_NOTE
	sed -i "s/#KERNEL_NAME#/${KERNEL_SOURCE_NAME}/g" $EX_NOTE $IN_NOTE
	sed -i "s/#NEW_PATCH_NAME#/${NEW_PATCH_FULL_NAME}/g" $EX_NOTE $IN_NOTE
	sed -i "s/#KERNEL_PACKAGE_NAME#/${KERNEL_PACKAGE_NAME}/g" $EX_NOTE $IN_NOTE
	case $KERNEL_PACKAGE_TYPE in
		'rpm' )
			:
			;;
		'dsc' )
			sed -i "s/#KERNEL_DSC_NAME#/${TSC_KERNEL_PACKAGE_NAME}/g" $EX_NOTE $IN_NOTE
			sed -i "s/#KERNEL_DEBIAN_NAME#/${TSC_DEBIAN_PACKAGE_NAEM}/g" $EX_NOTE $IN_NOTE
			;;
		'tar' )
			case $TOOLS_TYPE in
				'apt' )
					sed -i "s/#INSTALL_FUNC#/apt/g" $EX_NOTE $IN_NOTE
					sed -i "s/#INSTALL_TOOLS#/$apt_tools/g" $EX_NOTE $IN_NOTE
					;;
				'yum' )
					sed -i "s/#INSTALL_FUNC#/yum/g" $EX_NOTE $IN_NOTE
					sed -i "s/#INSTALL_TOOLS#/$yum_tools/g" $EX_NOTE $IN_NOTE
					;;
				'no' )
					if [ $(which apt) ];then
						sed -i "s/#INSTALL_FUNC#/apt/g" $EX_NOTE $IN_NOTE
						sed -i "s/#INSTALL_TOOLS#/$apt_tools/g" $EX_NOTE $IN_NOTE
					elif [ $(which yum) ];then
						sed -i "s/#INSTALL_FUNC#/yum/g" $EX_NOTE $IN_NOTE
						sed -i "s/#INSTALL_TOOLS#/$yum_tools/g" $EX_NOTE $IN_NOTE
					fi
					;;
			esac
			;;
		'zip' )
			case $TOOLS_TYPE in
				'apt' )
					sed -i "s/#INSTALL_FUNC#/apt/g" $EX_NOTE $IN_NOTE
					sed -i "s/#INSTALL_TOOLS#/$apt_tools/g" $EX_NOTE $IN_NOTE
					;;
				'yum' )
					sed -i "s/#INSTALL_FUNC#/yum/g" $EX_NOTE $IN_NOTE
					sed -i "s/#INSTALL_TOOLS#/$yum_tools/g" $EX_NOTE $IN_NOTE
					;;
				'no' )
					if [ $(which apt) ];then
						sed -i "s/#INSTALL_FUNC#/apt/g" $EX_NOTE $IN_NOTE
						sed -i "s/#INSTALL_TOOLS#/$apt_tools/g" $EX_NOTE $IN_NOTE
					elif [ $(which yum) ];then
						sed -i "s/#INSTALL_FUNC#/yum/g" $EX_NOTE $IN_NOTE
						sed -i "s/#INSTALL_TOOLS#/$yum_tools/g" $EX_NOTE $IN_NOTE
					fi
					;;
			esac
			;;
		'deb' )
			:
			;;
		* )
			exit 1
			;;
	esac
	cd ../
}

# 生成新的patch文件
renew_patch_file() {
	rm $NEW_PATCH_NAME/$NEW_PATCH_NAME/kernel_patch/All_Zhaoxin*
	cd ../kernel_orig
	KERNEL_SOURCE_NAME=$(ls)
	check_orig_and_rej
	update_origin_file
	diff -rNup $KERNEL_SOURCE_NAME ../$KERNEL_SOURCE_NAME > ../patch/$NEW_PATCH_NAME/$NEW_PATCH_NAME/kernel_patch/$NEW_PATCH_FULL_NAME
	cd ../patch/
}

# yum下载
yum_install() {
	echo "    #sudo yum install $yum_tools" >> $BUILD_GUIDE
}

# apt下载
apt_install() {
	echo "    #sudo apt install $apt_tools" >> $BUILD_GUIDE
}

# 制作build guide
renew_build_guide() {
	cd $NEW_PATCH_NAME/$NEW_PATCH_NAME/kernel_patch/
	echo "Patch Build guide" > $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	# part.1
	echo "1. Download source code">> $BUILD_GUIDE
	case $KERNEL_PACKAGE_TYPE in
		'rpm' )
			echo "    Download $OS_VERSION kernel source code: $KERNEL_PACKAGE_NAME" >> $BUILD_GUIDE
			echo "    #rpm2cpio $KERNEL_PACKAGE_NAME | cpio -div" >> $BUILD_GUIDE
			echo "    #tar -xvf $KERNEL_SOURCE_NAME.tar.xz" >> $BUILD_GUIDE
			;;
		'tar' )
			echo "    Download $OS_VERSION kernel source code: $KERNEL_PACKAGE_NAME" >> $BUILD_GUIDE
			echo "    #sudo tar -xvf $KERNEL_PACKAGE_NAME -C ~" >> $BUILD_GUIDE
			;;
		'zip' )
			echo "    Download $OS_VERSION kernel source code: $KERNEL_PACKAGE_NAME" >> $BUILD_GUIDE
			echo "    #sudo unzip $KERNEL_PACKAGE_NAME -d ~" >> $BUILD_GUIDE
			;;
		'dsc' )
			echo "    Download the 3 files form below links:" >> $BUILD_GUIDE
			echo "    http://http.debian.net/debian/pool/main/l/linux/$TSC_KERNEL_PACKAGE_NAME" >> $BUILD_GUIDE
			echo "    http://http.debian.net/debian/pool/main/l/linux/$TSC_DEBIAN_PACKAGE_NAEM" >> $BUILD_GUIDE
			echo "    http://http.debian.net/debian/pool/main/l/linux/$TSC_ORIG_PACKAGE_NAME" >> $BUILD_GUIDE
			echo -e >> $BUILD_GUIDE
			echo "    #sudo dpkg-source -x $TSC_KERNEL_PACKAGE_NAME" >> $BUILD_GUIDE
			;;
		'deb' )
			echo "    Download $OS_VERSION kernel source code: $KERNEL_PACKAGE_NAME" >> $BUILD_GUIDE
			echo "    #cd ~" >> $BUILD_GUIDE
			echo "    #wget http://archive.ubuntu.com/ubuntu/pool/main/l/linux/$KERNEL_PACKAGE_NAME" >> $BUILD_GUIDE
			echo "    #dpkg -x $KERNEL_PACKAGE_NAME ~" >> $BUILD_GUIDE
			echo "    #cp ~/usr/src/$KERNEL_SOURCE_NAME/${KERNEL_SOURCE_NAME}.tar.bz2 ~" >> $BUILD_GUIDE
			echo "    #tar -xvf ${KERNEL_SOURCE_NAME}.tar.bz2" >> $BUILD_GUIDE
			;;
		* )
			exit 1
			;;
	esac
	echo "    you would get kernel source code: ~/$KERNEL_SOURCE_NAME" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    Copy $NEW_PATCH_FULL_NAME to ~" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE

	# part.2
	echo "2. Source code + patch" >> $BUILD_GUIDE
	echo "    #cd ~/$KERNEL_SOURCE_NAME" >> $BUILD_GUIDE
	echo "    #patch -p1 < ../$NEW_PATCH_FULL_NAME" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE

	# part.3
	echo "3. Build steps:" >> $BUILD_GUIDE
		case $KERNEL_PACKAGE_TYPE in
		'rpm' )
			yum_install
			;;
		'tar' )
			case $TOOLS_TYPE in
				'apt' )
					apt_install
					;;
				'yum' )
					yum_install
					;;
				'no' )
					if [ $(which apt) ];then
						apt_install
					elif [ $(which yum) ];then
						yum_install
					fi
					;;
			esac
			;;
		'zip' )
			case $TOOLS_TYPE in
				'apt' )
					apt_install
					;;
				'yum' )
					yum_install
					;;
				'no' )
					if [ $(which apt) ];then
						apt_install
					elif [ $(which yum) ];then
						yum_install
					fi
					;;
			esac
			;;
		'dsc' )
				apt_install
			;;
		'deb' )
				apt_install
			;;
		* )
			exit 1
			;;
	esac
	echo -e >> $BUILD_GUIDE
	echo "    #cp /boot/config-xxx .config" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    make menuconfig" >> $BUILD_GUIDE
	echo "        Load .config and Config the following items looks like this: " >> $BUILD_GUIDE
	echo "            <*> Serial ATA and Parallel ATA drivers (libata)  ---> " >>$BUILD_GUIDE
	echo "                <*> Zhaoxin SATA support" >> $BUILD_GUIDE
	echo "                <*> VIA SATA support" >> $BUILD_GUIDE
	echo "            <M> Zhaoxin HW Random Number Generator support" >> $BUILD_GUIDE
	echo "            <M> Zhaoxin CPU temperature sensor" >> $BUILD_GUIDE
	echo "            <M> Support for Zhaoxin ACE" >> $BUILD_GUIDE
	echo "                <M> Zhaoxin ACE driver for AES algorithm" >> $BUILD_GUIDE
	echo "                <M> Zhaoxin ACE driver for SHA1 and SHA256 algorithms" >> $BUILD_GUIDE
	echo "            ()  Additional X.509 keys for default system keyring" >> $BUILD_GUIDE
	echo "        and save it;" >> $BUILD_GUIDE
	echo "        You can also select what you want to config if you need." >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    #make -j8" >> $BUILD_GUIDE
	echo "    #sudo make modules_install" >> $BUILD_GUIDE
	echo "    #sudo make install" >> $BUILD_GUIDE
	echo "    #reboot" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	# part.4
	echo "4. Check the patch:" >> $BUILD_GUIDE
	echo "    after reboot from new $KERNEL_SOURCE_NAME, check the patch:" >> $BUILD_GUIDE
	echo "    4.1 #dmesg | grep \"Patch Version\"" >> $BUILD_GUIDE
	echo "        you should see \"Linux Patch Version is V$PATCH_VERSION\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.2 #dmesg | grep \"CPU patch\"" >> $BUILD_GUIDE
	echo "        you should see \"With CPU patch V2.0.0\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.3 #dmesg | grep \"UHCI\"" >> $BUILD_GUIDE
	echo "        you should see \"Adjust the UHCI Controllers bit value patch V1.0.0\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.4 #dmesg | grep \"XHCI\"" >> $BUILD_GUIDE
	echo "        only for CND001, you should see \"XHCI QUIRK: Resetting on resume patch V1.0.0\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.5 #dmesg | grep \"HDAC\"" >> $BUILD_GUIDE
	echo "        you should see \"HDAC: Support HDAC ID patch V1.0.0\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "        when BIOS set HDAC Non-snoop Enable," >> $BUILD_GUIDE
	echo "        #dmesg | grep \"snoop\"" >> $BUILD_GUIDE
	echo "        you should see \"HDAC non-snoop patch V1.0.0\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.6 #dmesg | grep \"VIA\"" >> $BUILD_GUIDE
	echo "        you should not see the VIA word" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.7 #dmesg | grep \"DAC\"" >> $BUILD_GUIDE
	echo "        you should not see the \"disabling DAC on VIA PCI bridge\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.8 when BIOS set SATA in IDE mode, system should works normal." >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.9 #dmesg | grep \"topology\"" >> $BUILD_GUIDE
	echo "        you should see \"Got CPU topology from cpuid B leaf, 4 leaf patch V1.0.0\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.10 #lsmod | grep -i \"via\"" >> $BUILD_GUIDE
	echo "        you should not see the via word" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.11 when BIOS set \"CPU Processor C State\" to \"C2\" or higher State." >> $BUILD_GUIDE
	echo "        #dmesg | grep \"TSC\"" >> $BUILD_GUIDE
	echo "        you should see \"With TSC patch V1.0.0\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.12 #dmesg | grep \"MCE\"" >> $BUILD_GUIDE
	echo "        you should see \"MCE driver patch V1.0.2\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.13 #dmesg | grep \"GFX\"" >> $BUILD_GUIDE
	echo "        you should see \"HDAC: GFX HDAC patch V2.0.1\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.14 when BIOS enabled \"MCA Supported\"" >> $BUILD_GUIDE
	echo "        #dmesg | grep \"SVAD\"" >> $BUILD_GUIDE
	echo "        only for CHX001, you should see \"Disable MC8 SVAD error patch\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.15 #dmesg | grep \"WBINVD\"" >> $BUILD_GUIDE
	echo "        you should see \"Disable WBINVD operation when entering C3 or above patch V1.0.0\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.16 #dmesg | grep \"xHCI TRB\"" >> $BUILD_GUIDE
	echo "        on Zhaoxin CND001/CND003 platform, you should see \"With xHCI TRB prefetch patch V1.0.0\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.17 #dmesg | grep \"xHCI\"" >> $BUILD_GUIDE
	echo "        on Zhaoxin CND003/CHX002 platform, you should see \"With xHCI root hub speed patch V1.0.0\"" >> $BUILD_GUIDE
	echo -e >> $BUILD_GUIDE
	echo "    4.18 #cat /proc/cpuinfo | grep \"bugs\"" >> $BUILD_GUIDE
	echo "        you should not see \"spectre_v2\" & \"swapgs\"" >> $BUILD_GUIDE
}

# 制作pciids guide
renew_pciids_guide() {
	cd ../pciids_patch/
	if [ -f pci.ids ];then
		rm pci.ids
	fi
	echo "Patch use guide" > $PCIIDS_GUIDE
	echo -e >> $PCIIDS_GUIDE
	echo "1. You can download pci.ids form http://pciids.sourceforge.net/v2.2/pci.ids" >> $PCIIDS_GUIDE
	echo -e >> $PCIIDS_GUIDE
	echo "2. Use pci.ids override /usr/share/hwdata/pci.ids." >> $PCIIDS_GUIDE
	echo -e >> $PCIIDS_GUIDE
	echo "3. Use #lspci command to verify devices\`s description." >> $PCIIDS_GUIDE
	echo "    you should see \"VIA\" for 1106 VID; \"Zhaoxin\" for 1d17 VID." >> $PCIIDS_GUIDE
}

# 更新文件
update_file() {
	sed -i "s/$1/$2/g" $ROOT_DIR/$KERNEL_SOURCE_NAME/$3
}

# 修改源码文件中的内容
update_origin_file() {
	# 1. 修改patch版本号
	LINE=$(grep -nri "#define ZX_PATCH_VERSION" $ROOT_DIR/$KERNEL_SOURCE_NAME/arch/x86/kernel/cpu/zhaoxin.c | cut -d ":" -f1)
	sed -i "$((LINE))c #define ZX_PATCH_VERSION \"V$PATCH_VERSION\"" $ROOT_DIR/$KERNEL_SOURCE_NAME/arch/x86/kernel/cpu/zhaoxin.c
	LINE=$(grep -nri "#define ZX_PATCH_VERSION" $ROOT_DIR/$KERNEL_SOURCE_NAME/arch/x86/kernel/cpu/centaur.c | cut -d ":" -f1)
	sed -i "$((LINE))c #define ZX_PATCH_VERSION \"V$PATCH_VERSION\"" $ROOT_DIR/$KERNEL_SOURCE_NAME/arch/x86/kernel/cpu/centaur.c

	# 2. PadLock patch
	update_file "Zhaoxin PadLock ACE" "Zhaoxin ACE" drivers/crypto/Kconfig
	update_file "Zhaoxin PadLock driver" "Zhaoxin ACE driver" drivers/crypto/Kconfig
	update_file "Zhaoxin PadLock" "Zhaoxin ACE" drivers/crypto/Kconfig

	update_file "Zhaoxin PadLock ACE" "Zhaoxin ACE" drivers/crypto/zhaoxin-aes.c
	update_file "Zhaoxin PadLock" "Zhaoxin ACE" drivers/crypto/zhaoxin-aes.c

	update_file "Zhaoxin PadLock ACE" "Zhaoxin ACE" drivers/crypto/zhaoxin-sha.c
	update_file "Zhaoxin PadLock" "Zhaoxin ACE" drivers/crypto/zhaoxin-sha.c
	
	# 3. 检查Zhaoxin字符
	# 3.1 arch/x86/kernel/cpu/centaur.c
	if [ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/arch/x86/kernel/cpu/centaur.c ];then
		update_file "Zhaoxin Linux Patch Version is" "Linux Patch Version is" arch/x86/kernel/cpu/centaur.c
		update_file "With Zhaoxin Shanghai CPU patch" "With CPU patch" arch/x86/kernel/cpu/centaur.c
		update_file "Got Zhaoxin CPU topology" "Got CPU topology" arch/x86/kernel/cpu/centaur.c
	fi
	
	# 3.2 arch/x86/kernel/cpu/zhaoxin.c
	if [ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/arch/x86/kernel/cpu/zhaoxin.c ];then
		update_file "Zhaoxin Linux Patch Version is" "Linux Patch Version is" arch/x86/kernel/cpu/zhaoxin.c
		update_file "With Zhaoxin Shanghai CPU patch" "With CPU patch" arch/x86/kernel/cpu/zhaoxin.c
		update_file "Got Zhaoxin CPU topology" "Got CPU topology" arch/x86/kernel/cpu/zhaoxin.c
	fi
	
	# 3.3 arch/x86/kernel/cpu/mce/core.c & arch/x86/kernel/cpu/mcheck/mce.c
	if [ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/arch/x86/kernel/cpu/mce/core.c ];then
		update_file "Disable Zhaoxin CHX001 MC8 SVAD" "Disable MC8 SVAD" arch/x86/kernel/cpu/mce/core.c
		update_file "Zhaoxin MCE driver patch" "MCE driver patch" arch/x86/kernel/cpu/mce/core.c
	elif [ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/arch/x86/kernel/cpu/mcheck/mce.c ];then
		update_file "Disable Zhaoxin CHX001 MC8 SVAD" "Disable MC8 SVAD" arch/x86/kernel/cpu/mcheck/mce.c
		update_file "Zhaoxin MCE driver patch" "MCE driver patch" arch/x86/kernel/cpu/mcheck/mce.c
	fi
	
	# 3.4 drivers/char/hw_random/zhaoxin-rng.c
	if	[ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/drivers/char/hw_random/zhaoxin-rng.c ];then
		update_file "cannot enable Zhaoxin RNG" "cannot enable RNG" drivers/char/hw_random/zhaoxin-rng.c
		update_file "Zhaoxin RNG detected" "RNG detected" drivers/char/hw_random/zhaoxin-rng.c
	fi

	# 3.5 drivers/crypto/zhaoxin-sha.c
	if [ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/drivers/crypto/zhaoxin-sha.c ];then
		update_file "Zhaoxin ACE" "ACE" drivers/crypto/zhaoxin-aes.c
		update_file "KERN_NOTICE PFX" "KERN_NOTICE" drivers/crypto/zhaoxin-aes.c
		update_file "KERN_ERR PFX" "KERN_ERR" drivers/crypto/zhaoxin-aes.c
	fi

	# 3.6 drivers/crypto/zhaoxin-sha.c
	if [ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/drivers/crypto/zhaoxin-sha.c ];then
		update_file "Zhaoxin ACE" "ACE" drivers/crypto/zhaoxin-sha.c
		update_file "KERN_NOTICE PFX" "KERN_NOTICE" drivers/crypto/zhaoxin-sha.c
		update_file "KERN_ERR PFX" "KERN_ERR" drivers/crypto/zhaoxin-sha.c
	fi

	# 3.7 drivers/hwmon/zhaoxin-cputemp.c
	if [ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/drivers/hwmon/zhaoxin-cputemp.c ];then
		update_file "\"zhaoxin_cputemp\"" "\"cputemp\"" drivers/hwmon/zhaoxin-cputemp.c
	fi

	# 3.8 drivers/usb/host/xhci.c
	if [ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/drivers/usb/host/xhci.c ];then
		update_file "With Zhaoxin xHCI root hub speed patch" "With xHCI root hub speed patch" drivers/usb/host/xhci.c
	fi

	# 3.9 drivers/usb/host/xhci-mem.c
	if [ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/drivers/usb/host/xhci-mem.c ];then
		update_file "With Zhaoxin xHCI TRB prefetch patch" "With xHCI TRB prefetch patch" drivers/usb/host/xhci-mem.c
	fi

	# 3.10 sound/pci/hda/hda_intel.c & patch_hdmi.c
	if [ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/sound/pci/hda/hda_intel.c ];then
		update_file "\"HDA Zhaoxin\"" "\"HDA\"" sound/pci/hda/hda_intel.c
		update_file "\"HDA Zhaoxin GFX\"" "\"HDA GFX\"" sound/pci/hda/hda_intel.c
		update_file "zx_hda no chx001 device" "hda can't get pci_dev" sound/pci/hda/hda_intel.c
		update_file "zx_hda can't get pci_dev" "hda can't get pci_dev" sound/pci/hda/hda_intel.c
		update_file "Zhaoxin HDAC non-snoop patch" "HDAC non-snoop patch" sound/pci/hda/hda_intel.c
		update_file "Support Zhaoxin HDAC ID patch" "Support HDAC ID patch" sound/pci/hda/hda_intel.c
		update_file "Zhaoxin GFX HDAC patch" "GFX HDAC patch" sound/pci/hda/hda_intel.c
		update_file "zx_hda patch version" "hda patch version" sound/pci/hda/patch_hdmi.c
		update_file "zx_hda %x %p" "hda %x %p" sound/pci/hda/hda_intel.c
	fi

	# 3.11 arch/x86/events/zhaoxin/core.c
	if [ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/arch/x86/events/zhaoxin/core.c ];then
		update_file "elcome to zhaoxin pmu" "elcome to pmu" arch/x86/events/zhaoxin/core.c
		update_file "\"zhaoxin\"" "\"\"" arch/x86/events/zhaoxin/core.c
		update_file "ZXC events" "C events" arch/x86/events/zhaoxin/core.c
		update_file "ZXD events" "D events" arch/x86/events/zhaoxin/core.c
		update_file "ZXE events" "E events" arch/x86/events/zhaoxin/core.c
	elif [ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/arch/x86/kernel/cpu/perf_event_zhaoxin/core.c ];then
		update_file "elcome to zhaoxin pmu" "elcome to pmu" arch/x86/kernel/cpu/perf_event_zhaoxin/core.c
		update_file "\"zhaoxin\"" "\"\"" arch/x86/kernel/cpu/perf_event_zhaoxin/core.c
		update_file "ZXC events" "C events" arch/x86/kernel/cpu/perf_event_zhaoxin/core.c
		update_file "ZXD events" "D events" arch/x86/kernel/cpu/perf_event_zhaoxin/core.c
		update_file "ZXE events" "E events" arch/x86/kernel/cpu/perf_event_zhaoxin/core.c
	fi

	# 3.12 arch/x86/events/zhaoxin/uncore.c
	if [ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/arch/x86/events/zhaoxin/uncore.c ];then
		update_file "elcome to zhaoxin uncore" "elcome to uncore" arch/x86/events/zhaoxin/uncore.c
		update_file "zhaoxin uncore init success" "uncore init success" arch/x86/events/zhaoxin/uncore.c
	elif [ -f $ROOT_DIR/$KERNEL_SOURCE_NAME/arch/x86/kernel/cpu/perf_event_zhaoxin/uncore.c ];then
		update_file "elcome to zhaoxin uncore" "elcome to uncore" arch/x86/kernel/cpu/perf_event_zhaoxin/uncore.c
		update_file "zhaoxin uncore init success" "uncore init success" arch/x86/kernel/cpu/perf_event_zhaoxin/uncore.c
	fi

	# 3.13 arch/x86/kernel/acpi/cstate.c
	update_file "entering C3 or above for Zhaoxin CPUs patch" "entering C3 or above patch" arch/x86/kernel/acpi/cstate.c

	# 3.14 drivers/acpi/processor_idle.c
	update_file "With Zhaoxin TSC patch" "With TSC patch" drivers/acpi/processor_idle.c

}

# 检查冗余文件
check_orig_and_rej() {
	REJ_FILE=$(find $ROOT_DIR/$KERNEL_SOURCE_NAME -name "*.rej")
	ORIG_FILE=$(find $ROOT_DIR/$KERNEL_SOURCE_NAME -name "*.orig")
	if [ "$REJ_FILE" ] || [ "$ORIG_FILE" ];then
		echo -e "$WARNING警告：$KERNEL_SOURCE_NAME中含有*.rej或*.orig文件$CC"
		echo "---------------------------------"
		if [ "$REJ_FILE" ];then
			find $ROOT_DIR/$KERNEL_SOURCE_NAME/ -name "*.rej"
		fi
		if [ "$ORIG_FILE" ];then
			find $ROOT_DIR/$KERNEL_SOURCE_NAME/ -name "*.orig"
		fi
		echo "---------------------------------"
		read -p "是否删除或退出?[d/q(uit)]:" TMP
		echo "-----------------------------------------------"
		case ${TMP:0:1} in
			d ) 
				find $ROOT_DIR/$KERNEL_NAME -name "*.rej" | xargs rm -rfv
				find $ROOT_DIR/$KERNEL_NAME -name "*.orig" | xargs rm -rfv
				echo "-----------------------------------------------"
				echo "*.rej或*.orig文件已经删除！将继续制作patch包。"
				echo "-----------------------------------------------"
				;;
			q )
				echo -e "$RED脚步将退出，本次未做任何修改，请检查*.orig和*.rej文件$CC"
				echo "============================================================"
				exit 1
				;;
			* )
				check_orig_and_rej
				echo "------------------------------------------------------------"
				;;
		esac
	fi
}

# 制作新的patch
creat_new_patch() {
	# 获取内核名字
	KERNEL_SOURCE_NAME=$(ls kernel_orig)

	# 制作patch
	cd patch
	mv $PATCH_NAME $NEW_PATCH_NAME 2> /dev/null
	mv $NEW_PATCH_NAME/All_Zhaoxin* $NEW_PATCH_NAME/$NEW_PATCH_NAME 2> /dev/null
	
	renew_release_note
	renew_patch_file
	renew_build_guide
	renew_pciids_guide
	cd $ROOT_DIR/patch
	rm *.zip > /dev/null
	check_tools zip
	zip -r $NEW_PATCH_NAME.zip $NEW_PATCH_NAME
}

# 输入新的镜像名、内核版本、补丁版本
new_patch_name() {
	read -p "是否更新patch的文件名? [y/n/q(uit)]:" TMP
	echo "------------------------------------------"
	case ${TMP:0:1} in
		y )
			echo -e "$WHITE输入新的patch信息：$CC"
			echo "------------------------------------------"
			read -p "镜像版本：" OS_VERSION
			read -p "内核版本：" KERNEL_VERSION
			read -p "补丁版本：" PATCH_VERSION
			echo "------------------------------------------------------------"
			;;
		n )
			TMP=${PATCH_NAME#*_for_}
			TMP=${TMP%_*}
			OS_VERSION=${TMP%_*}
			KERNEL_VERSION="${TMP#*_}"
			;;
		q )
			echo -e "$WHITE脚步退出！$CC"
			exit 1
			;;
		* )
			new_patch_name
			;;
	esac
	NEW_PATCH_NAME="All_Zhaoxin_Patch_for_${OS_VERSION}_${KERNEL_VERSION}_V${PATCH_VERSION}"
	NEW_PATCH_FULL_NAME="All_Zhaoxin_Patch_for_${OS_VERSION}_${KERNEL_VERSION}_V${PATCH_VERSION}.patch"
	EX_NOTE="Linux_Package_for_${OS_VERSION}_${KERNEL_VERSION}_External_Release_Notes.html"
	IN_NOTE="Linux_Package_for_${OS_VERSION}_${KERNEL_VERSION}_Internal_Release_Notes.html"
}

# 准备编译内核源码
prepare_build_kernel() {
	KERNEL_SOURCE_NAME=$(ls kernel_orig)
	if [ -d "kernel_build" ];then
		read -p "已经存在kernel_build目录，是否覆盖? [y/n/q(uit)]:" TMP
		echo "------------------------------------------------------------"
		case ${TMP:0:1} in
			y )
				echo -e "$WHITE已同步内核源码$CC"
				cp $KERNEL_SOURCE_NAME kernel_build/ -rf
				;;
			n )
				:
				;;
			q )
				echo -e "$WHITE脚步退出！$CC"
				exit 1
				;;
			* )
				prepare_build_kernel
				;;
		esac
	else
		mkdir kernel_build
		cp $KERNEL_SOURCE_NAME kernel_build/ -rf
	fi
}

# 配置内核源码
config_build_kernel() {
	# 1. 复制/boot/config*文件
	CONFIG_FILE=$(ls /boot/config* | sed -n 1p)
	echo $PASSWD | sudo -S cp $CONFIG_FILE kernel_build/$KERNEL_SOURCE_NAME/.config
	# 2. 修改.config文件
	LINE=$(sed -n '/CONFIG_ATA=/=' kernel_build/$KERNEL_SOURCE_NAME/.config)
	sed -i "$((LINE))cCONFIG_ATA=y" kernel_build/$KERNEL_SOURCE_NAME/.config

	LINE=$(sed -n '/CONFIG_SATA_VIA=/=' kernel_build/$KERNEL_SOURCE_NAME/.config)
	sed -i "$((LINE))cCONFIG_SATA_VIA=y" kernel_build/$KERNEL_SOURCE_NAME/.config
	sed -i "$((LINE))iCONFIG_SATA_ZHAOXIN=y" kernel_build/$KERNEL_SOURCE_NAME/.config

	LINE=$(sed -n '/CONFIG_CPU_SUP_ZHAOXIN=/=' kernel_build/$KERNEL_SOURCE_NAME/.config)
	if [ -e $LINE ];then
		LINE=$(sed -n '/CONFIG_CPU_SUP_CENTAUR=/=' kernel_build/$KERNEL_SOURCE_NAME/.config)
		sed -i "$((LINE))iCONFIG_CPU_SUP_ZHAOXIN=y" kernel_build/$KERNEL_SOURCE_NAME/.config
	else
		sed -i "$((LINE))cCONFIG_CPU_SUP_ZHAOXIN=y" kernel_build/$KERNEL_SOURCE_NAME/.config
	fi

	LINE=$(sed -n '/CONFIG_HW_RANDOM_VIA=/=' kernel_build/$KERNEL_SOURCE_NAME/.config)
	sed -i "$((LINE))iCONFIG_HW_RANDOM_ZHAOXIN=m" kernel_build/$KERNEL_SOURCE_NAME/.config

	LINE=$(sed -n '/CONFIG_SENSORS_VIA_CPUTEMP=/=' kernel_build/$KERNEL_SOURCE_NAME/.config)
	sed -i "$((LINE))iCONFIG_SENSORS_ZHAOXIN_CPUTEMP=m" kernel_build/$KERNEL_SOURCE_NAME/.config

	LINE=$(sed -n '/CONFIG_CRYPTO_DEV_PADLOCK=/=' kernel_build/$KERNEL_SOURCE_NAME/.config)
	sed -i "$((LINE))iCONFIG_CRYPTO_DEV_ZHAOXIN=m" kernel_build/$KERNEL_SOURCE_NAME/.config
	sed -i "$((LINE+1))iCONFIG_CRYPTO_DEV_ZHAOXIN_AES=m" kernel_build/$KERNEL_SOURCE_NAME/.config
	sed -i "$((LINE+2))iCONFIG_CRYPTO_DEV_ZHAOXIN_SHA=m" kernel_build/$KERNEL_SOURCE_NAME/.config

	LINE=$(sed -n '/CONFIG_SYSTEM_TRUSTED_KEYS=/=' kernel_build/$KERNEL_SOURCE_NAME/.config)
	sed -i "$((LINE))cCONFIG_SYSTEM_TRUSTED_KEYS=\"\"" kernel_build/$KERNEL_SOURCE_NAME/.config
	# 更新.config文件
	cd kernel_build/$KERNEL_SOURCE_NAME
	make olddefconfig
}

# 编译内核
make_build_kernel() {
	make -j8 2>&1 | tee $ROOT_DIR/log/build1.log
	make -j8 2>&1 | tee $ROOT_DIR/log/build2.log
	if [ -f vmlinux.o ];then
		echo $PASSWD | sudo -S make modules_install 2>&1 | tee $ROOT_DIR/log/modules.log
		echo $PASSWD | sudo -S make install 2>&1 | tee $ROOT_DIR/log/install.log
	else
		echo -e "$ERROR编译出错，检查log!"
		exit 1
	fi
}

# 检查主板型号
check_base_board() {
	check_tools dmidecode
	TMP=$(echo $PASSWD | sudo -S dmidecode -t baseboard | grep "Product Name")
	if [[ $TMP =~ "HX002" ]];then
		BASE_BOARD="HX002"	
	elif [[ $TMP =~ "HX001" ]];then
		BASE_BOARD="HX001"	
	else 
		BASE_BOARD="all"
	fi
}

ARR1=("Patch Version"
      "CPU patch"
      "UHCI"
      "XHCI QUIRK"
      "HDAC"
      "snoop"
      "VIA"
      "DAC on VIA"
      "topology"
      "TSC patch"
      "MCE"
      "GFX"
      "SVAD"
      "WBINVD"
      "xHCI TRB"
      "xHCI root"
)

ARR2=("Linux Patch Version is"
      "With CPU patch"
      "Adjust the UHCI Controllers bit value patch"
      "XHCI QUIRK: Resetting on resume patch"
      "Support HDAC ID patch"
      "HDAC non-snoop patch"
      "nothing"
      "nothing"
      "Got CPU topology from..."
      "With TSC patch"
      "MCE driver patch"
      "GFX HDAC patch"
      "Disable MC8 SVAD error patch"
      "Disable WBINVD operation when entering..."
      "With xHCI TRB prefetch patch"
      "With xHCI root hub speed patch"
)

note_information() {
	COLOR=$RED

	if [ "${ARR1[$i]}" = "XHCI QUIRK" ];then
		if [ "$BASE_BOARD" = "HX002" ];then
			echo -e "${WARNING}NOTE$CC:此项应该为空"
			COLOR=$BULE #COLOR变量有两个作用：1.标志颜色 2.标志应该为空
		elif [ "$BASE_BOARD" = "HX001" ];then
			:
		else
			echo -e "${WARNING}NOTE$CC:Only for CND001"
			COLOR=$YELLOW
		fi
	fi
	
	if [ "${ARR1[$i]}" = "VIA" ];then
		echo -e "${WARNING}NOTE$CC:此项应该为空"
		COLOR=$BULE
	fi

	if [ "${ARR1[$i]}" = "DAC on VIA" ];then
		echo -e "${WARNING}NOTE$CC:此项应该为空"
		COLOR=$BULE
	fi

	if [ "${ARR1[$i]}" = "SVAD" ];then
		if [ "$BASE_BOARD" = "HX002" ];then
			echo -e "${WARNING}NOTE$CC:此项应该为空"
			COLOR=$BULE
		elif [ "$BASE_BOARD" = "HX001" ];then
			echo -e "${WARNING}NOTE$CC:Need to enable MCA Supported"
		else
			echo -e "${WARNING}NOTE$CC:Only for CHX001 and Need to enable MCA Supported"
			COLOR=$YELLOW
		fi
	fi

	if [ "${ARR1[$i]}" = "xHCI TRB" ];then
		if [ "$BASE_BOARD" = "HX002" ];then
			echo -e "${WARNING}NOTE$CC:此项应该为空"
			COLOR=$BULE
		elif [ "$BASE_BOARD" = "HX001" ];then
			:
		else
			echo -e "${WARNING}NOTE$CC:Only for CND001/CND003"
			COLOR=$YELLOW
		fi
	fi

	if [ "${ARR1[$i]}" = "xHCI root" ];then
		if [ "$BASE_BOARD" = "HX002" ];then
			:
		elif [ "$BASE_BOARD" = "HX001" ];then
			echo -e "${WARNING}NOTE$CC:此项应该为空"
			COLOR=$BULE
		else
			echo -e "${WARNING}NOTE$CC:Only for CND003/CHX002"
			COLOR=$YELLOW
		fi
	fi

	if [ "${ARR1[$i]}" = "snoop" ];then
		echo -e "${WARNING}NOTE$CC:Need to enable Non-snoop in BIOS"
	fi
	
	if [ "${ARR1[$i]}" = "TSC patch" ];then
		echo -e "${WARNING}NOTE$CC:Need to enable C State in BIOS"
	fi

	if [ "${ARR1[$i]}" = "WBINVD" ];then
		echo -e "${WARNING}NOTE$CC:Need to enable C State in BIOS"
	fi
}

# 验证patch
verify_dmesg() {
	cd kernel_orig
	KERNEL_SOURCE_NAME=$(ls)
	cd ..
	for (( i = 0 ; i< ${#ARR1[@]} ; i++ ))
	do
		echo "$(($i)). # dmesg | grep \"${ARR1[$i]}\""
		note_information 
		TMP=$(cat log/dmesg.log | grep "${ARR2[$i]}" | sed -n 1p )
		if [[ $TMP ]];then
			echo -e "==> ${WHITE}OK$CC:"$TMP
		else
			if [ "$COLOR" = "$BULE" ];then
				echo -e "==> ${WHITE}OK$COLOR:此项为空$CC"
			else
				let count++
				echo -e "==> $COLOR此项为空$CC"
			fi
		fi
		echo "------------------------------------------------------------"
	done

	echo "$(($i)). # cat /proc/cpuinfo | grep "bugs""
	echo -e "${WARNING}NOTE$CC:此项应该为空"
	TMP=$(sudo cat /proc/cpuinfo | grep "bugs" | sed -n 1p)
	if [[ $TMP =~ "spectre_v2" ]] || [[ $TMP =~ "swapgs" ]];then
		let count++
		echo -e "==> $RED$TMP$CC"
	else
		echo -e "==> ${WHITE}OK$BULE:此项为空$CC"
	fi
	echo "------------------------------------------------------------"
	
	# lsmod | grep -i "via"
	echo "$(($i+1)). # lsmod | grep -i "via""
	echo -e "${WARNING}NOTE$CC:此项应该为空"
	TMP=$(sudo lsmod | grep -i "via")
	if [ -z "$TMP" ];then
		echo -e "==> ${WHITE}OK$BULE:此项为空$CC"
	else
		let count++
		echo -e "==> $RED$TMP$CC"
	fi
	echo "------------------------------------------------------------"

	# pci-dma
	echo "$(($i+1)). # pci-dma.c检查"
	echo -e "${WARNING}NOTE$CC:此项应该为空"
	TMP=$(cat $ROOT_DIR/patch/All**/All**/kernel_patch/All** | grep "pci-dma" | sed -n 1p)
	if [ -z "$TMP" ];then
		echo -e "==> ${WHITE}OK$BULE:此项为空$CC"
	else
		let count++
		echo -e "==> $RED$TMP$CC"
	fi
	echo "------------------------------------------------------------"

	# cpu_detect
	echo "$(($i+2)). # cpu_detect_cache_size检查"
	echo -e "${WARNING}NOTE$CC:此项应该为空"
	TMP=$(cat $ROOT_DIR/$KERNEL_SOURCE_NAME/arch/x86/kernel/cpu/zhaoxin.c | grep "cpu_detect")
	if [ -z "$TMP" ];then
		echo -e "==> ${WHITE}OK$BULE:此项为空$CC"
	else
		let count++
		echo -e "==> $RED$TMP$CC"
	fi
	echo "------------------------------------------------------------"

	# padlock
	echo "$(($i+3)). # padlock检查"
	TMP=$(cat $ROOT_DIR/patch/All**/All**/kernel_patch/All** | grep "Zhaoxin PadLock" | sed -n 1p )
	if [ -z "$TMP" ];then
		echo -e "==> ${WHITE}OK$BULE:此项为空$CC"
	else
		let count++
		echo -e "==> $RED$TMP$CC"
	fi
	echo "------------------------------------------------------------"

	# cpufeatures.h 
	echo "$(($i+4)). # cpufeatures.h检查"
	TMP=$(cat $ROOT_DIR/patch/All**/All**/kernel_patch/All** | grep "ZX_FMA" | sed -n 1p )
	if [ -z "$TMP" ];then
		let count++
		echo -e "==> $RED此项为空$CC"
	else
		echo -e "==> ${WHITE}OK$CC:$TMP"
	fi
	echo "------------------------------------------------------------"
	
	# zhaoxin字符检查 
	echo "$(($i+5)). # zhaoxin字符检查"
	TMP=$(cat $ROOT_DIR/log/dmesg.log | grep "zhaoxin")
	TMP2=$(cat $ROOT_DIR/log/dmesg.log | grep "zx_hda")
	if [ -z "$TMP" ] || [ -z "$TMP2"];then
		echo -e "==> ${WHITE}OK$BULE:此项为空$CC"
	else
		let count++
		echo -e "==> $RED$TMP$TMP2$CC"
	fi
	echo "------------------------------------------------------------"

	if [ $count = "0" ];then
		echo -e "${BULE}检查OK，测试结果存放在patch.cat中$CC"
	else
		echo -e "${RED}检查到$count个错误，测试结果存放在patch.cat中$CC"
	fi
}

# 检查初始化文件内容
check_init_file() {
	if [ -d patch ] || [ -d package_backup ] || [ -d kernel_orig ];then
		echo "已经部署过项目,请检查项目"
		echo "============================================================"
		exit 0
	fi
	check_files .
	
}

#------------------------------------------------------------------------------------#
# 函数：arrange_project
# 参数：无
# 返值：无
# 功能：部署patch制作环境
# 		1. 获取内核，参考patch的信息
# 		2. 备份内核和参考patch
# 		3. 解压内核
# 		4. 给内核打patch
#------------------------------------------------------------------------------------#
arrange_project() {
	#1. 检查初始化文件
	check_init_file
	#2. 打印文件信息
	print_init_files
	#2. 备份文件
	backup_packages
	#3. 解压内核
	decompress_kernel
	#4. 打参考补丁
	patch_kernel
}

#------------------------------------------------------------------------------------#
# 函数：clean_project
# 参数：无
# 返值：无
# 功能：返回初始状态
#------------------------------------------------------------------------------------#
clean_project() {
	if [ -d package_backup ];then
		mv package_backup/* .
	fi
	for i in $(ls)
	do
		if [ -d $i ];then
			rm $i -rf
		fi
	done
	echo "清理完成!"
}

#------------------------------------------------------------------------------------#
# 函数：make_patch
# 参数：无
# 返值：无
# 功能：制作patch包
#		1. 获取内核源码名字
# 		2. 获取参考patch信息
# 		3. 制作patch主文件
#		4. 制作build_guide.txt pci_guide.txt等
#		5. 生成新的releasen note
#------------------------------------------------------------------------------------#
make_patch() {
	# 判断是否已经部署patch制作环境
	if [ -d "patch" ];then
		# 1 获取内核信息
		check_files package_backup
		# 2 获取当前patch信息
		check_files patch
		# 3 打印patch信息
		print_patch_files
		# 4 获取新的patch信息
		new_patch_name
		# 5 制作新的patch并打包
		creat_new_patch
	else	# 如未部署项目，打印信息并退出
		echo -e "$ERROR请先部署脚本环境"
	fi
	
}

#------------------------------------------------------------------------------------#
# check_build_tools
# 参数：无
# 返值：无
# 功能：检查编译所需软件是否已经安装
#------------------------------------------------------------------------------------#
check_build_tools() {
	if [ $(which apt) ];then
		for i in $apt_tools;
		do
			check_tools $i
		done
	elif [ $(which yum) ];then
		for i in $yum_tools;
		do
			check_tools $i
		done
	fi
}

#------------------------------------------------------------------------------------#
# build_project
# 参数：无
# 返值：无
# 功能：编译内核
#------------------------------------------------------------------------------------#
build_project() {
	# 1. 检查编译软件是否完备
	check_build_tools
	# 2. 准备待编译内核
	prepare_build_kernel
	# 3. 配置内核
	config_build_kernel
	# 4. 编译内核
	make_build_kernel
}

#------------------------------------------------------------------------------------#
# verify_patch
# 参数：无
# 返值：无
# 功能：验证内核
#------------------------------------------------------------------------------------#
verify_patch() {
	# 1. 获取主板型号
	check_base_board
	# 2. 获取dmesg启动log信息
	echo $PASSWD | sudo -S dmesg > log/dmesg.log
	# 3. 验证patch
	verify_dmesg
}

#------------------------------------------------------------------------------------#
# help_information
# 参数：无
# 返值：无
# 功能：打印帮助信息
#------------------------------------------------------------------------------------#
help_information() {
	echo "将目标内核源码包和参考patch包放到当前目录，然后执行下面脚本"
	echo "------------------------------------------------------------"
    echo "new.sh [option] [password]"
	echo "--------------------------"
    echo "    [option]: 必选"
    echo "        [a]rrange	:部署项目"
    echo "        [b]uild  	:编译内核"
    echo "        [c]lean  	:清空项目"
    echo "        [i]nspect :检查patch"
    echo "        [r]enew  	:重a建项目"
    echo "        [t]ools  	:检查软件"
    echo "        [p]atch  	:制作patch"
    echo "        [v]erify 	:校验patch"
    echo "        [h]elp   	:帮助信息"
	echo "   [password]: 可选"
	echo "        自动为sudo操作添加密码。"
	echo "        若没有该项，则password默认为:123"
	echo "------------------------------------------------------------"
}

inspect_patch() {
	vim patch/All_*/All*/kernel_patch/All*
}

renew_patch() {
	KERNEL_SOURCE_NAME=$(ls kernel_orig)
	rm $KERNEL_SOURCE_NAME -rf
	check_files package_backup
	case $KERNEL_PACKAGE_TYPE in 
		'tar' )
			tar -xf package_backup/$KERNEL_PACKAGE_NAME -C ./
			;;
		'zip' )
			unzip package_backup/$KERNEL_PACKAGE_NAME -d ./ >> /dev/null
			;;
		'dsc' )
			dpkg-source -x package_backup/$TSC_KERNEL_PACKAGE_NAME >> /dev/null
			rm $TSC_ORIG_PACKAGE_NAME
			;;
		'deb' )
			dpkg -x package_backup/$KERNEL_PACKAGE_NAME tmp >> /dev/null
			tar -xf tmp/usr/src/linux-source*/linux-source*.tar.* -C ./
			rm tmp/ -rf
			;;
		'rpm' )
			mkdir tmp
			cp package_backup/$KERNEL_PACKAGE_NAME tmp/
			cd tmp
			rpm2cpio $KERNEL_PACKAGE_NAME | cpio --quiet -di
			tar -xf linux*.tar.* -C ../
			cd ../
			rm tmp -rf
			;;
		'*' )
			:
			;;
	esac
	unzip package_backup/$PATCH_PACKAGE_NAME -d package_backup/
	cd $KERNEL_SOURCE_NAME
	patch -p1 < ../package_backup/$PATCH_NAME/All*/kernel_patch/All*
	cd ../package_backup
	rm $PATCH_NAME -rf
	cd ..
}

#====================================================================================#
# 函数：main
# 参数：$1:要执行的动作
# 返值：无
# 功能：主函数，用于执行指定动作
#		a:部署patch制作环境
#		b:编译并安装带patch内核
#		c:恢复初始状态
# 		p:制作patch
# 		t:安装需要的软件
#		v:验证patch
#====================================================================================#
main() {
	case $1 in
	a)
		echo "============================================================"
		echo -e "$WHITE部署项目$CC"
		echo "============================================================"
		arrange_project
		echo "============================================================"
		;;
	b)
		echo "============================================================"
		echo -e "$WHITE编译测试$CC"
		echo "============================================================"
		build_project
		echo "============================================================"
		;;
	c)
		echo "============================================================"
		echo -e "$WHITE清理项目$CC"
		echo "============================================================"
		clean_project
		echo "============================================================"
		;;
	i)
		echo "============================================================"
		echo -e "$WHITE检查patch$CC"
		echo "============================================================"
		inspect_patch
		echo "============================================================"
		;;
	r)
		echo "============================================================"
		echo -e "$WHITE重建项目$CC"
		echo "============================================================"
		renew_patch
		echo "============================================================"
		;;
	p)
		echo "============================================================"
		echo -e "$WHITE制作补丁$CC"
		echo "============================================================"
		make_patch
		echo "============================================================"
		;;
	v)
		echo "============================================================"
		echo -e "$WHITE验证补丁$CC"
		echo "============================================================"
		verify_patch
		echo "============================================================"
		;;
	t)
		echo "============================================================"
		echo -e "$WHITE安装工具$CC"
		echo "============================================================"
		check_build_tools
		echo "============================================================"
		;;
	h)
		echo "============================================================"
		echo -e "$WHITE帮助信息$CC"
		echo "============================================================"
		help_information
		echo "============================================================"
		;;
	*)
		echo "============================================================"
		help_information
		read -p "输入操作[a/b/c/r/t/p/v/q(uit)]:" TMP
		if [ ${TMP:0:1} = 'q' ];then
			echo "============================================================"
			echo "退出脚本！"
			echo "============================================================"
			exit
		else
			main $TMP
		fi
	esac
}

OPTION=$1
main ${OPTION:0:1} $2
